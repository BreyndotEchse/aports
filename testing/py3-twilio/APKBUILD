# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

_pyname=twilio
pkgname="py3-$_pyname"
pkgver=8.10.3
pkgrel=0
arch="noarch"
pkgdesc="Twilio API client and TwiML generator"
url="https://pypi.python.org/project/$_pyname"
license="MIT"
depends="
	py3-requests
	py3-mock
	py3-flake8
	py3-nose
	py3-python-jwt
	py3-twine
	py3-tz
	"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	py3-installer
	"
options="!check" # No testsuite
source="$pkgname-$pkgver.tar.gz::https://pypi.io/packages/source/${_pyname:0:1}/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
4f8623900c11073ed31e247920ac6662d7f9d7ce15a6e915a57266fdc97bc8e85b9496a3fba2d4e87861d9aff435179b33c7cbea41aed87d868326b417bf9670  py3-twilio-8.10.3.tar.gz
"
